#include <iostream>
#include <Kokkos_Core.hpp>
#include <Kokkos_Random.hpp>
#include <Kokkos_Sort.hpp>
#include <tuple>
#include <climits>
#include <Kokkos_StdAlgorithms.hpp>

#include <thrust/sort.h>

struct Triple {
  int x;
  int y;
  int z;
};

KOKKOS_INLINE_FUNCTION
bool operator<(const Triple &l, const Triple &r) {
  if (l.x < r.x)
    return true;
  if (l.x == r.x && l.y < r.y)
    return true;
  if (l.x == r.x && l.y == r.y && l.z < r.z)
    return true;
  return false;
}

KOKKOS_INLINE_FUNCTION
bool operator>(const Triple &l, const Triple &r) {
  return r < l;
}

KOKKOS_INLINE_FUNCTION
Triple operator-(const Triple &l, const Triple &r) {
  return {l.x-r.x, l.y-r.y, l.z-r.z};
}

KOKKOS_INLINE_FUNCTION
bool operator==(const Triple &l, const Triple &r) {
  return (l.x == r.x && l.y == r.y && l.z == r.z);
}

namespace Kokkos { //reduction identity must be defined in Kokkos namespace
   template<>
   struct reduction_identity<Triple > {
    KOKKOS_FORCEINLINE_FUNCTION constexpr static Triple max() {
      return {INT_MAX, INT_MAX, INT_MAX};
    }
    KOKKOS_FORCEINLINE_FUNCTION constexpr static Triple min() {
      return {INT_MIN, INT_MIN, INT_MIN};
    }
   };
}

int main(int argc, char** argv) {
  Kokkos::initialize(argc, argv);
  {
    // Max on RTX 3080 Ti 12GB ~5.1GB array
    // Set problem size
    const int N = 380*1000*1000*1.2;
    const int RR = 75;
    //const int N = 10;

    // Try sorting something small on host
    std::cout << "sorted A" << std::endl;
    int A[6] = {1, 4, 2, 8, 5, 7};
    thrust::sort(thrust::host, A, A + 6, thrust::greater<int>());
    for (int i = 0; i < 6; i++) {
      std::cout << A[i] << " ";
    }
    std::cout << std::endl;
    
    Kokkos::Random_XorShift64_Pool<> random_pool(12346);
    
    Kokkos::Timer timer;
    
    // Fill numbers array with random numbers
    Kokkos::View<int*> d_numbers("numbers", N);
    Kokkos::parallel_for(N, KOKKOS_LAMBDA(const int i) {
	auto generator = random_pool.get_state();
	int num = generator.rand(0,RR);
	d_numbers(i) = num;
	random_pool.free_state(generator);
      });
    
    
    timer.reset();
    Kokkos::sort(d_numbers);
    Kokkos::fence();
    double count_time = timer.seconds();
    std::cout << "time to sort " << N << " int: " << count_time << std::endl;
    
    decltype(d_numbers)::HostMirror h_numbers = Kokkos::create_mirror_view(d_numbers);

    timer.reset();
    Kokkos::deep_copy(h_numbers, d_numbers);
    Kokkos::fence();
    count_time = timer.seconds();
    std::cout << "time to copy: " << count_time << std::endl;
    std::cout << "first integers: " << std::endl;
    for (int i = 0; i < 10; i++) {
      std::cout << h_numbers(i) << std::endl;
    }
    std::cout << "last integers: " << std::endl;
    for (int i = N-11; i < N; i++) {
      std::cout << h_numbers(i) << std::endl;
    }
    // Verify sorted
    for (int i = 1; i < N; i++) {
      if (h_numbers(i-1) > h_numbers(i)) {
	std::cout << "Error: h_numbers(" << (i-1) << ") = " << h_numbers(i-1)
		  << " > " << "h_numbers(" << i << ") = " << h_numbers(i)
		  << std::endl;
	break;
      }
    }
    d_numbers = decltype(d_numbers)();

    Kokkos::View<Triple*> d_triples("triples", N);
    timer.reset();
    Kokkos::parallel_for(N, KOKKOS_LAMBDA(const int i) {
	auto generator = random_pool.get_state();
	int x = generator.rand(0,RR);
	int y = generator.rand(0,RR);
	int z = generator.rand(0,RR);
	d_triples(i).x = x;
	d_triples(i).y = y;
	d_triples(i).z = z;
	random_pool.free_state(generator);
      });
    double triples_fill_time = timer.seconds();
    std::cout << "time to write " << N << " triples: " << triples_fill_time
			       << std::endl;
    
    // Sort triples
    timer.reset();
    //Kokkos::sort(d_triples);
    thrust::sort(thrust::device, Kokkos::Experimental::begin(d_triples), Kokkos::Experimental::end(d_triples));
    double triples_sort_time = timer.seconds();
    std::cout << "time to sort " << N << " triples: " << triples_sort_time
	      << std::endl;
    // Copy triples to host
    decltype(d_triples)::HostMirror h_triples = Kokkos::create_mirror_view(d_triples);
    timer.reset();
    Kokkos::deep_copy(h_triples, d_triples);
    double triples_copy_time = timer.seconds();
    std::cout << "time to copy " << N << " triples from device: "
	      << triples_copy_time << std::endl;
    
    // Print 10 first triples
    std::cout << "10 first triples" << std::endl;
    for (int i = 0; i < 10; i++) {
      std::cout << h_triples(i).x << "," << h_triples(i).y << ","
		<< h_triples(i).z << std::endl;
    }
    
    // Print 10 last triples
    std::cout << "10 last triples" << std::endl;
    for (int i = N-11; i < N; i++) {
      std::cout << h_triples(i).x << "," << h_triples(i).y << ","
		<< h_triples(i).z << std::endl;
    }
    
    // Verify sorted
    for (int i = 1; i < N; i++) {
      Triple t1 = h_triples(i-1);
      Triple t2 = h_triples(i);
      if (t1 > t2) {
	std::cout << "Error: " << t1.x << "," << t1.y << "," << t1.z
		  << " > " << t2.x << "," << t2.y << "," << t2.z << std::endl;
	break;
      }
    }
  }
  Kokkos::finalize();
}
